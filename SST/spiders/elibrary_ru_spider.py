# -*- coding: utf-8 -*-

import scrapy
from SST.items import ArticleItem
import re


class ElibrarySpider(scrapy.Spider):
    """Crawler for the website elibrary.ru that contains information on
    Russian scientific articles."""
    name = "elibrary"
    # we use as startpage the advanced query form.
    start_urls = ["http://elibrary.ru/querybox.asp?scope=newquery"]

    def __init__(self, *args, **kwargs):
        """We initialize the spider, asking for the researched topic,
        according to the list (in cyrillic): http://elibrary.ru/rubrics.asp.
        """
        super(ElibrarySpider, self).__init__()

    def parse(self, response):
        """Parses the result list."""
        if re.search("newquery", response.url):
            # problème : il faut cliquer sur un bouton JavaScript
            request = scrapy.FormRequest. \
                from_response(response, formname='document.forms["results"]',
                              formdata={'authors_all': "",
                                        'begin_year': "2014",
                                        'changed': "1",
                                        'end_year': "2014",
                                        'ftext': "",
                                        'issues': "all",
                                        "order": "rev",
                                        "orderby": "rank",
                                        "querybox_name": "",
                                        "queryboxid": "0",
                                        "queryid": "",
                                        "rubrics_all": "020000|",
                                        "save_queryboxid": "0",
                                        "search_itemboxid": "",
                                        "search_morph": "on",
                                        "titles_all": "",
                                        "type_article": "on",
                                        "type_book": "on",
                                        "type_conf": "on",
                                        "type_disser": "on",
                                        "type_patent": "on",
                                        "type_preprint": "on",
                                        "type_report": "on",
                                        "where_abstract": "on",
                                        "where_keywords": "on",
                                        "where_name": "on",
                                        },
                              )
            yield request
        else:
            print response.url

    def parse_items(self, response):
        """Parses the results."""
