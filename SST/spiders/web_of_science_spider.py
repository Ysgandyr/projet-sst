# -*- coding: utf-8 -*-

import time
import re
import scrapy
from SST.items import ArticleItem
from scrapy.spiders.init import InitSpider
import logging
# from scrapy.linkextractors import LinkExtractor


class WebOfScienceSpider(InitSpider):
    """docstring for WebOfScienceSpider"""
    name = 'WebOfScience'
    login_page = "https://access.webofknowledge.com/user/userLogin.do"
    search_page = "http://apps.webofknowledge.com"
    start_urls = ["http://apps.webofknowledge.com/WOS_AdvancedSearch_input.do?SID=U1E762zByEP5xkLnEpC&product=WOS&search_mode=AdvancedSearch"]

    def __init__(self, username="", password="", year=2014, country="Russia*",
                 *args, **kwargs):
        """Le constructeur de cette spider. On prend en arguments le nom
        d'utilisateur et le mot de passe sur WOS ainsi que l'année recherchée.
        """
        super(scrapy.Spider, self).__init__(*args, **kwargs)
        self.http_user = username
        self.http_pass = password
        self.country = country
        self.year = year

    def init_request(self):
        """Fonction appelée à l'initialisation. On fait le login."""
        return scrapy.Request(url=self.login_page,
                              callback=self.login, dont_filter=True)

    def login(self, response):
        """Cette fonction génère une requête de login de l'utilisateur
        quand on arrive sur access.webofknowledge.com."""
        request = scrapy.FormRequest.\
            from_response(response,
                          formdata={'j_username': "X2013",
                                    'j_password': "!2013X",
                                    'j_auth_type': "UNP",
                                    'rememberme': "ON",
                                    'userType': "user"
                                    },
                          callback=self.check_login_response
                          )
        print "login request url : ", request.url
        return request

    def check_login_response(self, response):
        """On vérifie la réponse du login. Normalement, on devrait passer
        d'une page en access.webofknowledge.com à une page en
        apps.webofknowledge.com. Seulement, il y a plusieurs redirections pour
        faire cela sur un navigateur...
        J'essaye de forcer la redirection en accédant directement à la bonne
        page après le login, dans self.start_search.
        Problème : je ne sais pas comment vérifier le login."""
        print "login_response response url : ", response.url
        # Vérification du login après quelques secondes pour laisser le temps
        # aux redirections.
        time.sleep(10)
        print "check_login_url : ", response.url
        if re.search("loginFailed", response.url):
            self.logger.warning("Le login a echoué. Réessayez.")
        # Sinon, on continue. On est maintenant sur la page de formulaire.
        else:
            # print response.body
            if "Basic Search" in response.body:
                self.logger.warning("Succès ! Je règle les paramètres de recherche.")
                return scrapy.Request(callback=self.start_search)

    def start_search(self, response):
        """Cette fonction choisit la base de données "Core Collection" et
        passe en Recherche Avancée. Ensuite, elle interroge la base de données
        pour une année donnée au constructeur de WebOfScienceSpider.
        Enfin, elle renvoie la première page des résultats et appelle
        parse_results.
        """
        print "La recherche commence..."
        # Le problème vient d'ici : response.url donne
        # access.webofknowledge.com, donc le login ou la redirection ont
        # échoué. [Alors que request.url dans check_login_response donne
        # apps.webofknowledge.com]
        print "start_search response url : ", response.url
        # D'abord on clique sur la Core DB de WOS

        # Puis on sélectionne la recherche avancée

        return self.initialized()

    def parse(self, response):
        """Cette fonction contient la logique du parsing après initialisation.
        Elle est appelée directement après celle-ci. La fonction ajoute tous
        les liens vers les articles aux liens à parser et passe à la page
        suivante.
        On devrait se trouver sur la page des résultats."""
        links = response.xpath(restrict_xpaths=("//a[contains('RECORD',id)]/value"))
        for link in links:
            print link

    def parse_page(self, response):
        """Cette fonction traite la page d'un article. Elle choisit les
        informations pertinentes et construit un objet WebOfScienceItem.
        """
        item = ArticleItem()
        item['title'] = response.xpath("//h1")
        yield item
