# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ArticleItem(scrapy.Item):
    """Class representing an article on Web of Science"""
    title = scrapy.Field()
    authors = scrapy.Field
    institutions = scrapy.Field()
    source = scrapy.Field()
    funding = scrapy.Field()
    cited_references = scrapy.Field()
    keywords = scrapy.Field()


class ClassName(object):
    """docstring for ClassName"""
    def __init__(self, arg):
        super(ClassName, self).__init__()
        self.arg = arg
